const { UnAuthorizedException } = require('./http-excptions')
module.exports = class BaseController {

    /**
     * throw a unauthorized http exception. 
     * @param {Express.Response} res Express response object
     * @param {string} message Error Message
     * @param {string} code Unique identifier for the exception.
     */
    throwUnAuthorizedError(res, message, code = null) {
        res.status(401)
            .send(new UnAuthorizedException(message, code));
    }
}