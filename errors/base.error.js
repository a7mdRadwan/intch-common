module.exports = class BaseError {
    /**
     * The base error class.
     * @param {String} message The error message.
     * @param {String} type A message type for the error message.
     * @param {String} code A unique code to identify the error.
     */
    constructor(message, code = null) {
        /** Error message */
        this.message = message;
        /** Error unique code identifier */
        this.code = code || null;
    }
}