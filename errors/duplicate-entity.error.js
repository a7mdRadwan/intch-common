const BaseError = require('./base.error');
module.exports = class DuplicateEntityError extends BaseError {
    constructor(message, code) {
        super(message, code);
    }
}
