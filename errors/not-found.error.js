const BaseError= require('./base.error');

module.exports = class NotFoundError extends BaseError {
    /**
     * Initiate new not found error.
     * @param {string} message Error Message.
     * @param {string} code Unique identifier for the error.
     */
    constructor(message, code) {
        super(message, code);
    }
}