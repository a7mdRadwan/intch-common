const BaseError = require('./base.error');

module.exports = class ParameterError extends BaseError {
    /**
     * Parameter error to validate something
     * @param {string[]} params Parameter names
     * @param {string} message Error message
     * @param {string} code Then unique code of the error to identify.
     */
    constructor(params, message, type, code) {
        super(message, code);
        /** The http status code */
        this.statusCode = 400;
        /** The Parameters has errors */
        this.parameters = params;
    }    
}
