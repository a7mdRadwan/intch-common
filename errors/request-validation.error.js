module.exports = class RequestValidationError extends Error {
    /**
     * @param {{msg: string; param: string;}[]} errors Validation errors
     */
    constructor(errors) {
        /** The http status code */
        this.statusCode = 400;
        super();
        /** The errors array */
        this.errors = errors;
        // Error is a built-in class
        Object.setPrototypeOf(this, RequestValidationError.prototype);
    }

    serializeErrors() {
        return this.errors.map(e => ({ message: e.msg, field: e.param }));
    }
}