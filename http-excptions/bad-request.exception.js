const HttpExceptionBase = require('./http-exceptions-base');

module.exports = class BadRequestException extends HttpExceptionBase {
    /**
     * Creates new http bad request exception.
     * @param {string} message 
     * @param {string} code 
     */
    constructor(message, code) {
        super(400, message, code);
    }
}