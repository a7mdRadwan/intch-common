const HttpExceptionsBase = require("./http-exceptions-base");

module.exports = class ConflictException extends HttpExceptionsBase {
    /**
     * Initialzie Conflict http exception object.
     * @param {string} message The exception message.
     * @param {string} code A unique identifier for the exception occured.
     */
    constructor(message, code) {
        super(409, message, code);
    }
}