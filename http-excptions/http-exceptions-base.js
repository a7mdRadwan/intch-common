module.exports = class HttpExceptionsBase {
    /**
     * Initialize new Http Error Object.
     * @param {number} statusCode The http status code indecator.
     * @param {string} message The error message.
     * @param {string} code An unique identifier for the excpetion occured.
     */
    constructor(statusCode, message, code = null) {
        /** @type {number} */
        this.statusCode = statusCode;
        /** @type {string} */
        this.message = message;
        /** @type {string} */
        this.code = code;
    }
}