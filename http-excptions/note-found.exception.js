const HttpExceptionBase = require('./http-exceptions-base');

module.exports = class NotFoundExcpetion extends HttpExceptionBase {
    /**
     * Creates new http bad request exception.
     * @param {string} message 
     * @param {string} code 
     */
    constructor(message, code) {
        super(401, message, code);
    }
}