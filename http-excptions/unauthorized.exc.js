const HttpExceptionsBase = require('./http-exceptions-base');

module.exports = class UnAuthorizedException extends HttpExceptionsBase {
    /**
     * Initiate new http unauthorized exception.
     * @param {string} message Error message
     * @param {string} code Unique identifier for the error
     */
    constructor(message, code) {
        super(401, message, code);
    }
}  