const express = require('express');
const RequestValidationError = require('./../errors/request-validation.error');
const ParameterError = require('./../errors/parameter.error');
const NotFoundError = require('./../errors/not-found.error');
const DuplicateEntityError = require('./../errors/duplicate-entity.error');
const ConflictException = require('./../http-excptions/conflict.exc');
const HttpExceptionsBase = require('./../http-excptions/http-exceptions-base');
const BadRequestException = require('./../http-excptions/bad-request.exception');
const NotFoundException = require('./../http-excptions/note-found.exception');

/**
 * 
 * @param {Error} err 
 * @param {express.Request} req 
 * @param {express.Response} res 
 * @param {express.NextFunction} next 
 */
const excuteHandler = (fn) => {
    return async (req, res, next) => {
        try {
            await fn(req, res);
        } catch (exc) {
            console.error(exc);
            if (exc) {
                if (exc instanceof RequestValidationError) {
                    return res.status(exc.statusCode)
                        .send({ errors: exc.serializeErrors() });
                } else if (exc instanceof ParameterError) {
                    return res.status(exc.statusCode)
                        .send(exc);
                } else if (exc instanceof DuplicateEntityError) {
                    const httpExc = new ConflictException(exc.message, exc.code);
                    return res.status(httpExc.statusCode)
                        .send(httpExc);
                } else if (exc instanceof NotFoundError) {
                    const httpExcp = new NotFoundException(exc.message, exc.code);
                    return res.status(httpExcp.statusCode)
                } else if (exc instanceof HttpExceptionsBase) {
                    return res.status(exc.statusCode)
                        .send(exc);
                } else {
                    return res.status(500)
                        .send({ errors: [{ message: 'something went wrong!!' }] });
                }
            }

            next();
        }
    }
}
module.exports = excuteHandler;
