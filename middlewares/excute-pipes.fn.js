const { pascalToCamel } = require('./../utils/functions');

/**
 * Function to handle the validation pipes middlewares.
 * @param {{ transform: (val) => void; }} pipe transform function
 * @returns {(req: import('express')).Request, res: import('express')).Response, next: import('express')).NextFunction) => void}
 */
module.exports = (pipe, container) => {
    return (req, res, next) => {
        if (pipe.transform == null) {
            const instanceName = pascalToCamel(pipe);
            pipe = container.resolve(instanceName);
        }
        req.body = pipe.transform(req.body);
        next();
    }
}