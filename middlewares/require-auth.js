const jwt = require('jsonwebtoken');
const { UnAuthorizedException } = require('./../http-excptions')
/**
 * 
 * @param {import('express').Request} req Express request object 
 * @param {import('express').Response} res Express response object
 * @param {import('express').NextFunction} next Express Next Function
 */
module.exports = jwtSecret => (req, res, next) => {
    let token = req.headers['authorization'];
    if (!token) {
        res.status(401).send(new UnAuthorizedException('InvalidToken', 'INvTOKN'));
    }

    if (token.startsWith('Bearer ')) {
        token = token.replace('Bearer ', '');
    }

    try {
        const decoded = jwt.verify(token, jwtSecret);
        req.currentUser = { id: decoded.userId, username: decoded.username, email: decoded.email };
        next();
    } catch (exc) {
        console.log(exc);
        if (exc instanceof jwt.TokenExpiredError) {
            return res.status(401).send(new UnAuthorizedException('token:expired', 'TOKN6EXP'));
        }
        return res.status(401).send(new UnAuthorizedException('InvalidToken', 'INvTOKN'));
    }
}
