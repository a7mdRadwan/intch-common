module.exports = (klass) => {
    const klassName = klass.name;
    const [firstChar] = klassName;
    return `${firstChar.toLowerCase()}${klassName.slice(1, klassName.length)}`
};
