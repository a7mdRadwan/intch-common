const { Stan } = require('node-nats-streaming');
module.exports = class Listener {

    /**
     * 
     * @param {Stan} client 
     * @param {string} subject 
     * @param {string} queueGroupName 
     */
    constructor(client, subject, queueGroupName) {
        this.client = client;
        this.subject = subject;
        this.queueGroupName = queueGroupName;
        this.ackWait = 5 * 1000;
    }
    subsciptionOptions() {
        return this.client
            .subscriptionOptions()
            .setManualAckMode(true)
            .setDeliverAllAvailable()
            .setAckWait(this.ackWait)
            .setDurableName(this.queueGroupName);

    }

    listen() {
        const subscription = this.client.subscribe(
            this.subject,
            this.queueGroupName,
            this.subsciptionOptions(),
        );

        subscription.on('message', (msg) => {
            console.log(`Message received: ${this.subject} / ${this.queueGroupName}`);
            const paredData = this.parseMessage(msg);
            this.onMessage(paredData, msg);
        })
    }

    parseMessage(msg) {
        const data = msg.getData();
        return typeof data === 'string'
            ? JSON.parse(data)
            : JSON.parse(data.toString('utf-8'));
    }
}