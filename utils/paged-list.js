/**
 *  @template T
 */
module.exports = class PagedList {
    /**
     * 
     * @param {Array<T>} collection 
     * @param {number | null} count 
     * @param {number} pageSize 
     * @param {number} pageIndex 
     */
    constructor(collection, count, pageSize, pageIndex) {
        this.collection = collection
        this.count = count;
        this.pageSize = pageSize;
        this.pageIndex = pageIndex;
    }
    /**
     * @template T
     * @returns {PagedList<T>} 
     */
    static build({ collection, count, pageSize, pageIndex }) {
        return new PagedList(collection, count, pageSize, pageIndex)
    }
}
